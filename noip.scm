#!/usr/bin/env gosh

(use srfi-8)
(use srfi-13)
(use srfi-60)
(use rfc.base64)
(use rfc.http)
(use sxml.ssax)
(use sxml.sxpath)
(use gauche.net)
(use gauche.parameter)
(use gauche.parseopt)
(use gauche.termios)

(define *user-agent* "noip.scm/0.1 pclouds@gmail.com")
(define *username* (make-parameter #f))
(define *password* (make-parameter #f))
(define *debug* (make-parameter #f))

(define (print-debug . args)
  (if (*debug*)
      (apply print args)))

(define (get-credential)                ; for requestL=
  (with-output-to-string
    (lambda ()
      (with-input-from-string
          (format "username=~a&pass=~a" (*username*) (*password*))
        base64-encode))))

(define (get-visible-address)
  (receive (code header body)
      (http-get "ip1.dynupdate.no-ip.com:8245" "/")
    (unless (string=? code "200")
      (error "failed to get visible address" code header body))
    (let ([ip (string-trim-both body)])
      (print-debug (format "visible IP is ~a" ip))
      ip)))

(define (get-interface-address name)
  (let ([sock (make-socket AF_INET SOCK_DGRAM)])
    (if (logtest (socket-ioctl sock SIOCGIFFLAGS name) IFF_UP)
        (sockaddr-addr (socket-ioctl sock SIOCGIFADDR name))
        #f)))

(define (%sxml->hosts sxml)
  (map (lambda (domain)
         (let ([domain-name (car ((sxpath "/@name" '()) domain))])
           (map (lambda (host-name)
                  (cons (cadr host-name)
                        (cadr domain-name)))
                ((sxpath "/host/@name" '()) domain))))
       ((sxpath "//domain" '()) sxml)))

(define (sxml->hosts sxml)
  (apply append (%sxml->hosts sxml)))

(define (get-hosts)
  (let* ([uri (format "/settings.php?requestL=~a" (get-credential))])
    (receive (code header body)
        (http-get "dynupdate.no-ip.com" uri)
      (unless (string=? code "200")
        (error "failed to get host list" code header body))
      (let ([sxml (ssax:xml->sxml (open-input-string body) '())])
        (sxml->hosts body)))))

(define (update-host host secure :optional (address #f))
  (let ([uri (if address
                 (format "/nic/update?hostname=~a&myip=~a"
                         host
                         (inet-address->string address AF_INET))
                 (format "/nic/update?hostname=~a" host))])
    (receive (code headers body)
        (http-get "dynupdate.no-ip.com" uri
                  :secure secure
                  :auth-user (*username*)
                  :auth-password (*password*))
      (unless (string=? code "200")
        (error "failed to update host" code headers body))
      (cond
       [(#/^(good|nochg) (.*)/ body)
        => (lambda (re)
             (let ([ip (string-trim-both (re 2))])
               (print-debug (format "updated IP to ~a" ip))
               ip))]
       [else
        (error "failed to update host" body)]))))

(define (main args)
  (let-args (cdr args)
      ([username "u|username=s"]
       [password "p|password=s"]
       [host "h|host=s"]
       [all-hosts "all-hosts"]
       [debug "debug"]
       [secure "secure"])
    (parameterize ([http-user-agent *user-agent*]
                   [*username* username]
                   [*password* (or password
                                   (begin
                                     (display "Enter password: ")
                                     (flush (current-output-port))
                                     (without-echoing #f read-line)))]
                   [*debug* debug])
      (let loop ([ip (update-host host secure)])
        (sys-sleep 300)
        (let ([new-ip (get-visible-address)])
          (if (string=? ip new-ip)
              (loop ip)
              (loop (update-host host secure))))))))
